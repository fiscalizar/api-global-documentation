# FiscalizAR - API - Global - Documentation

----------------------

## PROJECT INFORMATION

----------------------

[![pipeline status](https://gitlab.com/fiscalizar/api-global-documentation/badges/master/pipeline.svg)](https://gitlab.com/fiscalizar/api-global-documentation/commits/master)  [![coverage report](https://gitlab.com/fiscalizar/api-global-documentation/badges/master/coverage.svg)](https://gitlab.com/fiscalizar/api-global-documentation/commits/master)  <img src="http://online.swagger.io/validator?url=https://online.swagger.io/validator?url=https://fiscalizar.gitlab.io/api-global-documentation/swagger.yaml">

Swagger documentation of global API. See HTML documentation in
[https://fiscalizar.gitlab.io/api-global-documentation](https://fiscalizar.gitlab.io/api-global-documentation)

## CONFIGURATIONS

----------------------

Connected to [swagger hub](https://app.swaggerhub.com/apis/fiscalizar/Global-API). Each time documentation changes, it will be commit into this repository.

## MODEL DIAGRAM

----------------------

## CONTRIBUTING

----------------------

Contributions to this repository are very welcome.

To contribute, please fork this repository on GitLab and send a pull request with a clear description of your changes. If appropriate, please ensure that the user documentation in this README is updated.

If you have submitted a PR and not received any feedback for a while, feel free to [ping me on Twitter](https://twitter.com/patoperpetua) [or find me on facebook](https://www.facebook.com/pato.arg)

----------------------

## TODO

* [ ] Config tool to generate models diagram.
* [ ] Config client libraries on commit.
* [x] Config swagger hub integration.
* [x] Upload html documentation to gitlab pages.
* [ ] Upload html documentation to servers.

----------------------

## IMPROVES TO NEXT VERSION

----------------------

© [Singleton](http://www.singleton.com.ar), Argentina, 2019.